﻿using Microsoft.Xrm.Sdk;

namespace TimeEntrySplitPlugin.Extensions
{
    public static class IPluginExecutionContextExtension
    {
        public static Entity RetrieveEntity(this IPluginExecutionContext ctx)
        {
            return (ctx.InputParameters.Contains("Target") 
                && ctx.InputParameters["Target"] is Entity entity) 
                ? entity 
                : null;
        }
    }
}
