﻿using Microsoft.Xrm.Sdk;

namespace TimeEntrySplitPlugin.Extensions
{
    public static class EnityExtensions
    {
        public static T Clone<T>(this T entity) 
            where T : Entity
        {
            var clone = new Entity(entity.LogicalName);
            foreach (var attr in entity.Attributes)
            {
                if (attr.Key.ToLowerInvariant() == entity.LogicalName.ToLowerInvariant() + "id")
                    continue;
                clone[attr.Key] = attr.Value;
            }

            return clone.ToEntity<T>();
        }
    }
}
