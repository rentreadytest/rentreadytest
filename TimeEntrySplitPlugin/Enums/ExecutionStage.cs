﻿namespace TimeEntrySplitPlugin.Enums
{
    public enum ExecutionStage
    {
        PreValidation = 10,
        PreOperation = 20,
        MainOperation = 30,
        PostOperation = 40
    }
}
