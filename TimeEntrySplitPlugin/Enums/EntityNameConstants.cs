﻿namespace TimeEntrySplitPlugin.Enums
{
    public static class TimeEntryConstants
    {
        public const string LOGICAL_NAME = "msdyn_timeentry";
        public const string START_ATTR_NAME = "msdyn_start";
        public const string END_ATTR_NAME = "msdyn_end";
    }
}
