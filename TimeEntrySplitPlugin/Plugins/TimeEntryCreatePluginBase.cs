﻿using System;
using Microsoft.Xrm.Sdk;
using TimeEntrySplitPlugin.Enums;

namespace TimeEntrySplitPlugin.Plugins
{
    public abstract class TimeEntryCreatePluginBase
    {
        private const string ALLOWED_MESSAGE_NAME = "Create";

        public Entity GetEntityToProcessOrNull(IPluginExecutionContext ctx, ExecutionStage expectedStage)
        {
            if (ctx.Stage == (int)expectedStage
                && ctx.MessageName == ALLOWED_MESSAGE_NAME
                && ctx.InputParameters.Contains("Target")
                && ctx.InputParameters["Target"] is Entity timeEntry
                && timeEntry.LogicalName == TimeEntryConstants.LOGICAL_NAME)
                return timeEntry;

            return null;
        }
    }
}
