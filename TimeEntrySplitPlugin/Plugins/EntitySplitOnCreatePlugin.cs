﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using TimeEntrySplitPlugin.Enums;
using TimeEntrySplitPlugin.Extensions;

namespace TimeEntrySplitPlugin.Plugins
{
    public class EntitySplitOnCreatePlugin : 
        TimeEntryCreatePluginBase, 
        IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var ctx = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var orgService = factory.CreateOrganizationService(ctx.UserId);

            var entity = base.GetEntityToProcessOrNull(ctx, ExecutionStage.PostOperation);
            if (entity is null)
                return;

            var start = entity.GetAttributeValue<DateTime>(TimeEntryConstants.START_ATTR_NAME);
            var end = entity.GetAttributeValue<DateTime>(TimeEntryConstants.END_ATTR_NAME);
            if (start.Date == end.Date)
                return;

            KeepOnlyFirstDay(orgService, entity, start);
            var splitEntities = SplitSlotsToMultipleEntities(entity, start, end);
            CreateTimeEntries(orgService, splitEntities);
        }

        // TODO: batch save
        private static void CreateTimeEntries(IOrganizationService orgService, List<Entity> splitEntities)
        {
            foreach (var timeEntry in splitEntities)
                orgService.Create(timeEntry);
        }

        private static List<Entity> SplitSlotsToMultipleEntities(Entity originalEntity, DateTime start, DateTime end)
        {
            var splitEntityList = new List<Entity>();
            var currDate = start.Date.AddDays(1);
            while (currDate <= end.Date)
            {
                var dayEntity = originalEntity.Clone();
                if (currDate != end.Date)
                {
                    dayEntity[TimeEntryConstants.START_ATTR_NAME] = currDate.Date;
                    dayEntity[TimeEntryConstants.END_ATTR_NAME] = currDate.Date.AddDays(1).AddTicks(-1);
                }
                else
                {
                    dayEntity[TimeEntryConstants.START_ATTR_NAME] = end.Date;
                    dayEntity[TimeEntryConstants.END_ATTR_NAME] = end;
                }

                splitEntityList.Add(dayEntity);
                currDate = currDate.AddDays(1);
            }
            return splitEntityList;
        }

        private static void KeepOnlyFirstDay(IOrganizationService orgService, Entity entity, DateTime start)
        {
            entity[TimeEntryConstants.START_ATTR_NAME] = start;
            entity[TimeEntryConstants.END_ATTR_NAME] = start.Date.AddDays(1).AddTicks(-1);

            orgService.Update(entity);
        }
    }
}
