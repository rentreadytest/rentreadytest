﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using TimeEntrySplitPlugin.Enums;

namespace TimeEntrySplitPlugin.Plugins
{
    public class ValidateIntersectionsPlugin :
        TimeEntryCreatePluginBase,
        IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var ctx = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var orgService = factory.CreateOrganizationService(ctx.UserId);

            var entity = base.GetEntityToProcessOrNull(ctx, ExecutionStage.PreValidation);
            if (entity is null)
                return;

            var getIntersectionQuery = new QueryExpression(TimeEntryConstants.LOGICAL_NAME);
            getIntersectionQuery.ColumnSet.AddColumns(TimeEntryConstants.START_ATTR_NAME, TimeEntryConstants.END_ATTR_NAME);

            getIntersectionQuery.Criteria = new FilterExpression(LogicalOperator.Or);
            var filters = GetIntersectionFoundFilters(entity);
            foreach (FilterExpression filter in filters)
            {
                getIntersectionQuery.Criteria.AddFilter(filter);
            }

            var entryPaginatedList = orgService.RetrieveMultiple(getIntersectionQuery);
            if (entryPaginatedList.Entities.Any())
            {
                throw new InvalidPluginExecutionException("TimeSlot already busy, please select another timeslot");
            }
        }

        private FilterExpression[] GetIntersectionFoundFilters(Entity newEntity)
        {
            return new[] {
                GetExistedPartiallyInsideNewFitler(newEntity),
                GetBoundaryTouchFilter(newEntity),
                GetNewFullyInsideExistedFilter(newEntity)
            };
        }

        private static FilterExpression GetExistedPartiallyInsideNewFitler(Entity newEntity)
        {
            var filter = new FilterExpression(LogicalOperator.Or);

            filter.AddCondition(TimeEntryConstants.START_ATTR_NAME,
                ConditionOperator.Between,
                newEntity[TimeEntryConstants.START_ATTR_NAME],
                newEntity[TimeEntryConstants.END_ATTR_NAME]);
            filter.AddCondition(TimeEntryConstants.END_ATTR_NAME,
                ConditionOperator.Between,
                newEntity[TimeEntryConstants.START_ATTR_NAME],
                newEntity[TimeEntryConstants.END_ATTR_NAME]);

            return filter;
        }

        private static FilterExpression GetNewFullyInsideExistedFilter(Entity newEntity)
        {
            var filter = new FilterExpression(LogicalOperator.And);

            filter.AddCondition(TimeEntryConstants.START_ATTR_NAME,
                ConditionOperator.LessThan,
                newEntity[TimeEntryConstants.START_ATTR_NAME]);
            filter.AddCondition(TimeEntryConstants.END_ATTR_NAME,
                ConditionOperator.GreaterThan,
                newEntity[TimeEntryConstants.END_ATTR_NAME]);

            return filter;
        }

        private static FilterExpression GetBoundaryTouchFilter(Entity newEntity)
        {
            var filter = new FilterExpression(LogicalOperator.Or);

            filter.AddCondition(TimeEntryConstants.START_ATTR_NAME,
                ConditionOperator.Equal,
                newEntity[TimeEntryConstants.START_ATTR_NAME]);
            filter.AddCondition(TimeEntryConstants.START_ATTR_NAME,
                ConditionOperator.Equal,
                newEntity[TimeEntryConstants.END_ATTR_NAME]);
            filter.AddCondition(TimeEntryConstants.END_ATTR_NAME,
                ConditionOperator.Equal,
                newEntity[TimeEntryConstants.START_ATTR_NAME]);
            filter.AddCondition(TimeEntryConstants.END_ATTR_NAME,
                ConditionOperator.Equal,
                newEntity[TimeEntryConstants.END_ATTR_NAME]);

            return filter;
        }
    }
}
