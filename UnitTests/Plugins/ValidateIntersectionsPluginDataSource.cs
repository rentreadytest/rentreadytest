﻿using System;
using System.Collections.Generic;
using UnitTests.Models;

namespace UnitTests.Plugins
{
    public static class ValidateIntersectionsPluginDataSource
    {
        public static IEnumerable<object[]> TimeSlotIntersection =>
            new List<object[]> {
                new object[] {
                    // existed          |-----|
                    // new      |-----|         
                    "New entry before existed - validation success",
                    // existed
                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),
                    // new
                    new DateTime(2001, 01, 02, 09, 00, 00),
                    new DateTime(2001, 01, 02, 09, 30, 00),
                    // validation result
                    true
                },
                new object[] {
                    // existed     |-----|
                    // new      |--|             
                    "New entry end is the existed start - validation failed",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 09, 00, 00),
                    new DateTime(2001, 01, 02, 10, 00, 00),

                    false
                },
                new object[] {
                    // existed     |-----|
                    // new      |-----|        
                    "New entry end in the middle of existed - validation failed",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 09, 00, 00),
                    new DateTime(2001, 01, 02, 10, 00, 01),

                    false
                },
                new object[] {
                    // existed     |-----|
                    // new      |--------|        
                    "New entry end in the end of existed - validation failed",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 09, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    false
                },
                new object[] {
                    // existed     |-----|
                    // new      |----------|        
                    "New entry include existed- validation failed",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 09, 00, 00),
                    new DateTime(2001, 01, 02, 14, 00, 00),

                    false
                },
                new object[] {
                    // existed     |-----|
                    // new         |---|        
                    "New entry start as existed- validation failed",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 12, 00, 00),

                    false
                },
                new object[] {
                    // existed     |-----|
                    // new          |---|        
                    "New entry inside existed - validation failed",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 11, 00, 00),
                    new DateTime(2001, 01, 02, 12, 00, 00),

                    false
                },
                new object[] {
                    // existed     |-----|
                    // new           |---|        
                    "New entry end the same as existed end - validation failed",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 11, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    false
                },
                new object[] {
                    // existed     |-----|
                    // new           |-----|        
                    "New entry intersects - validation failed",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 11, 00, 00),
                    new DateTime(2001, 01, 02, 14, 00, 00),

                    false
                },
                new object[] {
                    // existed     |-----|
                    // new               |-----|        
                    "New entry start is existed`s end - validation failed",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 13, 00, 00),
                    new DateTime(2001, 01, 02, 14, 00, 00),

                    false
                },
                new object[] {
                    // existed     |-----|
                    // new                  |-----|        
                    "New entry after existed - validation success",

                    new DateTime(2001, 01, 02, 10, 00, 00),
                    new DateTime(2001, 01, 02, 13, 00, 00),

                    new DateTime(2001, 01, 02, 14, 00, 00),
                    new DateTime(2001, 01, 02, 15, 00, 00),

                    true
                }
            };
    }
}
