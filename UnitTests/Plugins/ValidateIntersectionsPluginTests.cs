﻿using FakeXrmEasy;
using FluentAssertions;
using Microsoft.Xrm.Sdk;
using System;
using System.Linq;
using TimeEntrySplitPlugin.Enums;
using TimeEntrySplitPlugin.Plugins;
using UnitTests.Models;
using Xunit;

namespace UnitTests.Plugins
{
    public class ValidateIntersectionsPluginTests
    {
        [Theory]
        [MemberData(nameof(ValidateIntersectionsPluginDataSource.TimeSlotIntersection),
            MemberType = typeof(ValidateIntersectionsPluginDataSource))]
        public void Execute_TimeSlotsIntersections_Should_Throw_Exception(
            string caseDescription,
            DateTime existedStart,
            DateTime existedEnd,
            DateTime newStart,
            DateTime newEnd,
            bool validationSuccessExpected)
        {
            var newEntry = CreateTimeEntry(newStart, newEnd);
            var existedEntry = CreateTimeEntry(existedStart, existedEnd, Guid.NewGuid());
            var ctx = new XrmFakedContext();
            ctx.Initialize(existedEntry);
            Action runPluginAction = () 
                => ctx.ExecutePluginWithTarget<ValidateIntersectionsPlugin>(newEntry, stage: (int)ExecutionStage.PreValidation);

            if (validationSuccessExpected)
            {
                runPluginAction.ShouldNotThrow(caseDescription);
            }
            else 
            {
                runPluginAction
                    .ShouldThrow<InvalidPluginExecutionException>(because: caseDescription)
                    .WithMessage("TimeSlot already busy, please select another timeslot");
            }
        }

        private static Entity CreateTimeEntry(DateTime start, DateTime end, Guid? id = null)
        {
            var target = new Entity(TimeEntryConstants.LOGICAL_NAME);
            target[TimeEntryConstants.START_ATTR_NAME] = start;
            target[TimeEntryConstants.END_ATTR_NAME] = end;
            if (id.HasValue)
                target.Id = id.Value;
            return target;
        }

        [Fact]
        public void Execute_WrongMessage_Should_Skip()
        {
            var target = CreateTimeEntry(DateTime.UtcNow, DateTime.UtcNow.AddDays(3), Guid.NewGuid());
            var ctx = new XrmFakedContext();
            ctx.Initialize(target);

            ctx.ExecutePluginWithTarget<ValidateIntersectionsPlugin>(target,
                messageName: "Update");

            ctx.CreateQuery(TimeEntryConstants.LOGICAL_NAME)
                .ToList().Should().HaveCount(1);
        }

        [Fact]
        public void Execute_WrongStage_Should_Skip()
        {
            var target = CreateTimeEntry(DateTime.UtcNow, DateTime.UtcNow.AddDays(3), Guid.NewGuid());
            var ctx = new XrmFakedContext();
            ctx.Initialize(target);

            ctx.ExecutePluginWithTarget<ValidateIntersectionsPlugin>(target,
                stage: (int)ExecutionStage.PostOperation);

            ctx.CreateQuery(TimeEntryConstants.LOGICAL_NAME)
                .ToList().Should().HaveCount(1);
        }

        [Fact]
        public void Execute_WrongEntity_Should_Skip()
        {
            var wrongEntityTarget = new Entity("account");
            var ctx = new XrmFakedContext();

            ctx.ExecutePluginWithTarget<ValidateIntersectionsPlugin>(wrongEntityTarget);

            ctx.CreateQuery("account")
                .ToList().Should().BeEmpty();
        }
    }
}
