﻿using System;
using System.Collections.Generic;
using UnitTests.Models;

namespace UnitTests.Plugins
{
    public static class EntitySplitOnCreatePluginDataSource
    {
        public static IEnumerable<object[]> MultipleDaySplit =>
            new List<object[]> {
                new object[] {
                    "If split is not necessary - keep single record",
                    new DateTime(2001, 02, 01, 10, 00, 00),
                    new DateTime(2001, 02, 01, 10, 30, 00),
                    new TimeEntryModel[] { 
                        new TimeEntryModel { 
                            Start = new DateTime(2001, 02, 01, 10, 00, 00),
                            End = new DateTime(2001, 02, 01, 10, 30, 00)
                        }
                    }
                },
                new object[] {
                    "Day boundary exceeded - split",
                    new DateTime(2001, 02, 01, 23, 00, 00),
                    new DateTime(2001, 02, 02, 01, 15, 16),
                    new TimeEntryModel[] {
                        new TimeEntryModel {
                            Start = new DateTime(2001, 02, 01, 23, 00, 00),
                            End = new DateTime(2001, 02, 01, 23, 59, 59)
                        },
                        new TimeEntryModel {
                            Start = new DateTime(2001, 02, 02, 00, 00, 00),
                            End = new DateTime(2001, 02, 02, 01, 15, 16)
                        }
                    }
                },
                new object[] {
                    "More than 2 days difference - end of the day, full day, the first part of the day",
                    new DateTime(2001, 02, 01, 21, 00, 00),
                    new DateTime(2001, 02, 04, 02, 00, 00),
                    new TimeEntryModel[] {
                        new TimeEntryModel {
                            Start = new DateTime(2001, 02, 01, 21, 00, 00),
                            End = new DateTime(2001, 02, 01, 23, 59, 59)
                        },
                        new TimeEntryModel {
                            Start = new DateTime(2001, 02, 02, 00, 00, 00),
                            End = new DateTime(2001, 02, 02, 23, 59, 59)
                        },
                        new TimeEntryModel {
                            Start = new DateTime(2001, 02, 03, 00, 00, 00),
                            End = new DateTime(2001, 02, 03, 23, 59, 59)
                        },
                        new TimeEntryModel {
                            Start = new DateTime(2001, 02, 04, 00, 00, 00),
                            End = new DateTime(2001, 02, 04, 02, 00, 00)
                        }
                    }
                },
            };
    }
}
