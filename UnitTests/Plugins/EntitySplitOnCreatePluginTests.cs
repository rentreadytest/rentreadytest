﻿using FakeXrmEasy;
using FluentAssertions;
using Microsoft.Xrm.Sdk;
using System;
using System.Linq;
using TimeEntrySplitPlugin.Enums;
using TimeEntrySplitPlugin.Plugins;
using UnitTests.Models;
using Xunit;

namespace UnitTests.Plugins
{
    public class EntitySplitOnCreatePluginTests
    {
        [Theory]
        [MemberData(nameof(EntitySplitOnCreatePluginDataSource.MultipleDaySplit),
            MemberType = typeof(EntitySplitOnCreatePluginDataSource))]
        public void Execute_DifferentDates_Should_Split(
            string caseDescription,
            DateTime start, 
            DateTime end, 
            TimeEntryModel[] expectedSplit)
        {
            var ctx = new XrmFakedContext();
            var target = SetupTimeEntryTarget(start, end, ctx);

            var fakedPlugin = ctx.ExecutePluginWithTarget<EntitySplitOnCreatePlugin>(target);

            var splitEntries = ctx.CreateQuery(TimeEntryConstants.LOGICAL_NAME)
                .Select(TimeEntryModel.Map).ToList();
            splitEntries.ShouldBeEquivalentTo(expectedSplit,
                options =>
                    options.Using<DateTime>(c => c.Subject.Should()
                            .BeCloseTo(c.Expectation, 1000))
                    .WhenTypeIs<DateTime>(),
                caseDescription);
        }

        private static Entity SetupTimeEntryTarget(DateTime start, DateTime end, XrmFakedContext ctx)
        {
            var target = new Entity(TimeEntryConstants.LOGICAL_NAME);
            target[TimeEntryConstants.START_ATTR_NAME] = start;
            target[TimeEntryConstants.END_ATTR_NAME] = end;
            
            var service = ctx.GetOrganizationService();
            target.Id = service.Create(target);

            return target;
        }

        [Fact]
        public void Execute_WrongMessage_Should_Skip()
        {
            var ctx = new XrmFakedContext();
            var target = SetupTimeEntryTarget(DateTime.UtcNow, DateTime.UtcNow.AddDays(3), ctx);

            ctx.ExecutePluginWithTarget<EntitySplitOnCreatePlugin>(target,
                messageName: "Update");

            ctx.CreateQuery(TimeEntryConstants.LOGICAL_NAME)
                .ToList().Should().HaveCount(1);
        }

        [Fact]
        public void Execute_WrongStage_Should_Skip()
        {
            var ctx = new XrmFakedContext();
            var target = SetupTimeEntryTarget(DateTime.UtcNow, DateTime.UtcNow.AddDays(3), ctx);

            ctx.ExecutePluginWithTarget<EntitySplitOnCreatePlugin>(target,
                stage: (int)ExecutionStage.PreOperation);

            ctx.CreateQuery(TimeEntryConstants.LOGICAL_NAME)
                .ToList().Should().HaveCount(1);
        }

        [Fact]
        public void Execute_WrongEntity_Should_Skip()
        {
            var wrongEntityTarget = new Entity("account");
            var ctx = new XrmFakedContext();

            ctx.ExecutePluginWithTarget<EntitySplitOnCreatePlugin>(wrongEntityTarget);

            ctx.CreateQuery("account")
                .ToList().Should().BeEmpty();
        }
    }
}
