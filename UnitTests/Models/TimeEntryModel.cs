﻿using Microsoft.Xrm.Sdk;
using System;
using TimeEntrySplitPlugin.Enums;

namespace UnitTests.Models
{
    public class TimeEntryModel
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        // TODO: AutoMapper
        public static TimeEntryModel Map(Entity entity)
        {
            return new TimeEntryModel
            {
                Start = entity.GetAttributeValue<DateTime>(TimeEntryConstants.START_ATTR_NAME),
                End = entity.GetAttributeValue<DateTime>(TimeEntryConstants.END_ATTR_NAME)
            };
        }

        public static Entity Map(TimeEntryModel model)
        {
            var entity = new Entity(TimeEntryConstants.LOGICAL_NAME);
            entity[TimeEntryConstants.START_ATTR_NAME] = model.Start;
            entity[TimeEntryConstants.END_ATTR_NAME] = model.End;
            return entity;
        }
    }
}
