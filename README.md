# README #

### Уточнения про реализацию логики ###

В процессе реализации у меня возникли вопросы насчёт описанной логики. 
При обычном процессе я бы пошёл прояснить эти моменты у PO. 
А пока что я реализовал так, как понимаю сам, но заложил возможность быстрых изменений при необходимости (изменение фильтров валидации и отдельный плагин).

* *On creation of a Time Entry record the plugin should evaluate if the start and end date contain different values from each other.* 

Согласно описанию я должен разделять сущности/записи на несколько, если дата отличается. То есть если диапазон находится более чем в одном дне. Но тут возникает вопрос, когда кончается день. По умолчанию я решил работать с UTC и новый день это UTC 00:00:00.

* *In the event that the start and end date are different then a time entry record should be created for every date in the date range from start to end date.*

Я должен создать запись для каждого для, но мне кажется это немного странным - итоговое время таким образом увеличится. Вместо это я разделил на 2 записи и оставил время прежнее.
Пример. Есть интервал 2021.01.03 23:00:00 - 2021.01.04 02:00:00 UTC. Он должен быть разделён на 2 интервала 2021.01.03 23:00:00 - 2021.01.03 23:59:59 и 2021.01.04 00:00:00 - 2021.01.04 02:00:00. Таким образом общее время зарезервированное на активность останется прежним.

* *The plugin should also ensure that there are no duplicate time entry records created per date*

Эту проверку я бы тоже уточнил у PO. Насколько я понял, созданные слоты времени назначаются на человека и предотвращение дубликатов позволит убрать из расписания нереальные задачи/резервы времени.
Однако время может не только дублироваться, а ещё пересекаться. Например зарезервировано следущее время 10:00:00 - 12:00:00, и если мы попытаемся зарезервировать 10:30:00 - 11:00:00 это не будет являться дубликатом, хотя похоже, что такое не должно быть позволено в системе.
На данный момент я не учитывал конкретных сотрудников для назначения.
Также сейчас эта валидация проходит на этапе PreValidation, что не покрывает случаи race condition и большой нагрузки, когда такие дубликаты будут просачиваться. Но это выглядит как отдельная более серьёзная проблема (описана ниже как предложенное улучшение).

### Причины выбора plugin over azure functions ###

У плагинов D365 из коробки поддерживаются транзакции, по крайней мере для Pre/PostOperation. Учитывая, что приходится разделять сущности на несколько это крайне полезно.
Судя по документации интеграция с AzureFunction идёт через WebHooks, тут возникает неоднозначность - как себя вести, если AF сломалась. 
Нужно ли делать retry и если да, то как долго он может происходить. Было бы удобно использовать очередь перед AF для обработки, но тогда придётся добавлять DeadLetterQueue, 
что ещё усложнит структуру. Кроме того общение через AF подразумевает большие задержки. А поскольку данные в итоге всё равно пойдут в D365, встроенного механизма должно быть вполне достаточно.
Поскольку изначально на встрече обсуждали, что я могу выбрать то, что мне больше подойдёт я решил написать плагин.

### Описание плагинов ###

* EntitySplitOnCreatePlugin.cs - плагин разделяет интервалы времени, если они слишком большие. Регистируется на PostOperation (async). Исходно созданная запись редактируется, дополнительные создаются.
* ValidateIntersectionsPlugin.cs - плагин валидирует, что при создании нет пересечений с другими интервалами. Регистрируется на PreValidation.

### Желательные улучшения, выходящие за рамки требований и сроков ###

* Уточнение требований насчёт разделения сущностей. Граница разделений на данный момент по UTC, но возможно это должно быть в отдельной специальной таймзоне исполнителя.
* Сохранять новые сущности не по одной. Скорее всего есть какой нибудь BatchSave.
* Покрыть тестами Extensions.
* Возможно более UserFriendly сообщение для пользователя.
* Race condition fix with optimistic concurrency, но это требует углубления во внутренности D365, в частности в уровни изоляций транзацкий на этапах Pre/Post/Main операций. 
	Remark: Есть подозрение, что 2 операции могут паралельно зачитать (даже в рамках транзакции), что дубликатов нет, и потом создать дублирующие функции. Решил описать этот момент, он не усложнять реализации без согласования.
* Специальный технический пользователь с привилегиями для исполнения плагина.
* Возможно есть декларативное представление для более удобной регистрации планигов через CI/CD (CloudFormation alike).
* Также почитав описания Plugin vs AzureFunctions было принято решение для данной конкретной задачи использовать именно Plugins.

### Timeline (Total 12h) ###

* 2h - Читал и разбирался что такое D365 и система плагинов.
* 1h - Настройка триала D365.
* 0.5h - Знакомство с Field Service.
* 0.5h - Превый плагин (HelloWorld)
* 1h - RegistrationTool проблемы с авторизацей, баги - подтверждается топиками на офф сайте.
* 4h - Разработка 2х плагинов, дебаг и ручное тестирование.
* 2h - Покрытие тестами.
* 1h - Cleanup.
